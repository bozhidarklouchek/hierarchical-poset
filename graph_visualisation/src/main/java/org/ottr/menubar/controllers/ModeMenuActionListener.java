package org.ottr.menubar.controllers;

import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.models.representation.NestedGraphTree;
import org.ottr.graph.views.MainView;
import org.ottr.menubar.views.GraphMenu;

import javax.swing.*;
import java.awt.*;

/**
 * Manages the events from the mode menu option.
 */
public class ModeMenuActionListener
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public ModeMenuActionListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    /**
     * The PICKING mode is associated with the {@link ModalGraphMouse.Mode#PICKING} mode of the graph mouse, where the
     * user can select vertices (by clicking, shift-clicking, or dragging after having clicked for multi-select) or
     * edges (by clicking and shift-clicking). Currently, may not select both vertices and edges with the reasoning that
     * there isn't defined behaviour for selecting both. In this mode the user may navigate hierarchical levels.
     * Listener for the Picking radio button from the menu view {@link GraphMenu#getModeMenuPickingRadioButton()}.
     */
    public void selectModePicking()
    {
        graphModel.getMode().setPickingMode();

        DefaultModalGraphMouse<String, Number> graphMouse = new DefaultModalGraphMouse<>();
        graphMouse.setMode(ModalGraphMouse.Mode.PICKING);
        mainView.getVisualizationViewer().setGraphMouse(graphMouse);

        mainView.getRightSideScreen().getPreviewPanel().setVisible(false);

        // If parent above, make back button appear
        if(graphModel.getCurrentGraph().getParent() != null)
        {
            mainView.getRightSideScreen().getGoUpButton().setVisible(true);
        }
        else
        {
            mainView.getRightSideScreen().getGoUpButton().setVisible(false);
        }

        // Clear preview
        mainView.getRightSideScreen().getPreviewPanel().removeAll();
        // Placeholder text
        JPanel preview_box_text_box = new JPanel();
        preview_box_text_box.setLayout(new GridBagLayout());
        preview_box_text_box.setBackground(Color.decode("#D3D3D3"));
        preview_box_text_box.add(new JLabel("Select nested vertex to preview."));
        mainView.getRightSideScreen().getPreviewPanel().add(preview_box_text_box, BorderLayout.CENTER);
    }

    /**
     * The TRANSFORMING mode is associated with the {@link ModalGraphMouse.Mode#TRANSFORMING} mode of the graph mouse,
     * where the user can perform geometrical operations on the graph - translate, shear, rotate, etc.
     * Listener for the Transforming radio button from the menu view
     * {@link GraphMenu#getModeMenuTransformingRadioButton()}.
     */
    public void selectModeTransforming()
    {
        graphModel.getMode().setTransformingMode();

        DefaultModalGraphMouse<String, Number> graphMouse = new DefaultModalGraphMouse<>();
        graphMouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);
        mainView.getVisualizationViewer().setGraphMouse(graphMouse);

        mainView.getRightSideScreen().getPreviewPanel().setVisible(false);
        mainView.getRightSideScreen().getMergeButton().setVisible(false);
        mainView.getRightSideScreen().getGoUpButton().setVisible(false);
        mainView.getRightSideScreen().getGoDownButton().setVisible(false);

        // Clear preview
        mainView.getRightSideScreen().getPreviewPanel().removeAll();
        // Placeholder text
        JPanel preview_box_text_box = new JPanel();
        preview_box_text_box.setLayout(new GridBagLayout());
        preview_box_text_box.setBackground(Color.decode("#D3D3D3"));
        preview_box_text_box.add(new JLabel("Select nested vertex to preview."));
        mainView.getRightSideScreen().getPreviewPanel().add(preview_box_text_box, BorderLayout.CENTER);
    }

    /**
     * The PREVIEWING mode is associated with the {@link ModalGraphMouse.Mode#PICKING} mode of the graph mouse, so there
     * isn't a designated a mode in the graph mouse, and the previewing mode is completely custom. You cannot navigate
     * levels as the usual PICKING mode, but if you were to right-click a deep vertex, (refer to
     * {@link NestedGraphTree#isVertexDeep()}) a preview of the graph within the vertex is displayed in the preview box
     * on the right side screen. The preview functionality only works for one vertex at a time, so selecting multiple
     * vertices yields no result. Listener for the Previewing radio button from the menu view
     * {@link GraphMenu#getModeMenuPreviewingRadioButton()}.
     */
    public void selectModePreviewing()
    {
        graphModel.getMode().setPreviewingMode();

        DefaultModalGraphMouse<String, Number> graphMouse = new DefaultModalGraphMouse<>();
        graphMouse.setMode(ModalGraphMouse.Mode.PICKING);
        mainView.getVisualizationViewer().setGraphMouse(graphMouse);

        mainView.getRightSideScreen().getPreviewPanel().setVisible(true);
        mainView.getRightSideScreen().getMergeButton().setVisible(false);
        mainView.getRightSideScreen().getGoUpButton().setVisible(false);
        mainView.getRightSideScreen().getGoDownButton().setVisible(false);
    }
}
