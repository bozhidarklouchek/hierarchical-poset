package org.ottr;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.models.representation.NestedGraphTree;

import java.io.*;

/**
 * Loads data from JSON/serialised file.
 */
public class DataLoader
{
    /**
     * Reads graph data from given graph JSON file.
     * @param path is the path to the JSON file
     * @return the internal graph structure
     * @throws Exception for file I/O
     */
    public static NestedGraphTree initFromJSON(String path) throws Exception
    {

        JSONObject graph_json = null;
        JSONParser parser = new JSONParser();
        
		try
        {
			Object obj = parser.parse(new FileReader(path));
            graph_json = (JSONObject) obj;
		}
        catch (Exception e)
        {
			e.printStackTrace();
		}

        assert graph_json != null;


        return new NestedGraphTree(graph_json);
    }

    /**
     * Reads graph data from given serialised file.
     * @param path is the path to the serialised file
     * @return the internal graph structure
     * @throws IOException for file I/O
     * @throws ClassNotFoundException for problems with class serialisation
     */
    public static GraphModel initFromSave(String path) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(path);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        GraphModel model = (GraphModel) objectInputStream.readObject();
        objectInputStream.close();

        return model;
    }
}
