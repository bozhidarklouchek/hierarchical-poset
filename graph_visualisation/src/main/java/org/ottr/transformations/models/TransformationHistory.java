package org.ottr.transformations.models;

import java.io.Serializable;
import java.util.Stack;

/**
 * Represents the history of performed transformations.
 */
public class TransformationHistory implements Serializable
{
    private Stack<Transformation> completedTransformations;

    /**
     * Class constructor.
     */
    public TransformationHistory()
    {
        completedTransformations = new Stack<>();
    }

    /**
     * If the transformation history isn't empty, it pops the last transformation from it and returns it, usually done
     * when the user undoes the last transformation.
     * @return the last performed transformation
     */
    public Transformation popLastTransformation()
    {
        if(!completedTransformations.isEmpty())
        {
            return completedTransformations.pop();
        }

        return null;
    }

    /**
     * If the transformation history isn't empty, it peeks at the last transformation from it, usually done
     * to check what type it is.
     * @return the last performed transformation
     */
    public Transformation peekAtLastTransformation()
    {
        if(!completedTransformations.isEmpty())
        {
            return completedTransformations.peek();
        }

        return null;
    }

    /**
     * Checks if the transformation history is empty, usually done to allow the last transformation to be undone.
     * @return boolean stating if the transformation history is empty
     */
    public boolean isTransformationHistoryEmpty()
    {
        return completedTransformations.isEmpty();
    }

    /**
     * Adds a transformation to the history, usually done after the transformation has been complete and is recorded
     * so it may be undone later.
     * @param completedTransformation the transformation to be added to history
     */
    public void addTransformationToHistory(Transformation completedTransformation)
    {
        completedTransformations.add(completedTransformation);
    }
}
