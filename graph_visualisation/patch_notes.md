# PATCH NOTES
## Introduction
Here you will find documented changes in the development of the graph visualisation tools, including:

* Major changes
* Minor changes
* Assumptions throughout development

All changes here can be traced back to commits in the repository to inspect and/or gain insight as to when and how a feature has been implemented.
## Patch notes

### PATCH NOTES: 06/07/2022

**Major changes**
- Merge vertices
  - select vertices you want to merge (by shift-click and/or drag select)
  - merging button appears in place of "go down" button
  - merging merges the vertices on the level you're looking at and the level below
- Preview deeper ~~abstraction~~ hierarchical level (without actually navigating one level down)
  - introduction of preview mode, it adds the preview box (in place of button boxes)
  - can right-click deep vertex in preview mode to show deeper level in preview box
  - can not go forward/backwards in levels while in preview mode

**Minor changes**
- Remove .idea and target from git track
- Refactor JSON for clarity
- Refactor EXPAND/BACK navigation buttons to GO DOWN/GO UP
- Colour vertices in different colours to signify if selected/unselected , deep/not
- Add mnemonics/accelators for faster interaction with menu
- Order selected vertices id's in ascending order
- Downgrade to java version 17

**Assumptions**
1. GO DOWN/MERGE button in same place, reasoning: no way for them to exist at the same time (_**accepted**_)

2. Self loops aren't important (can be removed) (_**accepted**_)

3. When merging, new parent vertex will be first selected vertex (ascending if used drag select, first one otehrwise), reasoning: more control _**(to be changed**_ at a later date to have merged vertices have unique id's)

4. When exiting preview mode, the preview box is cleared (so if switch between preview and pick mode, the preview you just had open will be cleared), reasoning: confusing logic when you preview vertex A, merge A with and B in picking mode, now should the preview still keep showing A? or the merged A and B? If you decide to undo the transformation, what should the preview show now? A, B, or their old merged version? What if you're merging 100 vertices? (_**accepted**_)


### PATCH NOTES: 13/07/2022 (no release)

Major changes:
- Git workflow
  - switched to a main/dev git workflow
  - all main builds will be stable so can be checked to see how features have changed
  - all feature branches merged to dev first, and then after dev is clear of bugs it's merge into main
- Internal repesentation is now a recursive structure
  - solves the issue with going back/forwards stack problem, since now there are pointers to go back/forward
- Information objects as JSON key-value pairs
  - added with a vertical scroll bar in case too many pairs
  - indented and bolded format

Minor changes:
- Add patch notes doc to repository
- Start using issues to document current tasks
- Separate some main logic into different class logic and use of packages
- Remove vertices count from JSON
- Merged vertices are a different colour + unique id's
- Popup for transformation history
- File menu options

Assumptions:
- Can only select vertcies/edges (so can only merge vertices by selecting said vertcies, or selecting edge between two vertcies) (accpeted)
- Cannot select multiple edges and merge the vertices connected (might change later, but for now expected)
- info obj is unordered (from json) (need to introduce consistent order)

Next priorities:
- Transformation history (finalise)
- File menu


### PATCH NOTES: 20/07/2022

Major changes:
- MVC architectural pattern
- Transformation history
  - Can use ctrl+z to undo last transformation (currently only possible transformation is a merge)
  - if on the same screen as last transformation the merge gets undone and the merge is split
  - if on a different screen you get prompted with a dialog box to either undo it with/without shifting to the screen
  - if the current screen is the result of a merge on the level above, you'd be prompted with another dialog to move one level above
- FIle menu item with Open, Save, Save as options
  - Save saves the current state of the graph to the default save directory (serialised)
  - Open loads the saved state in the graph from the default save directory (deserialised)


Minor changes:
- Merging combines additional information JSONs
- Add ~~abstraction~~ hierarchical level counter on graph information screen
- Additional info JSON order is consistent

Assumptions:
- Default save directory is in target folder (which is git untracked), is that a good way to do this? reasoning: save states are specific to user + it's a resulting file which ive gathered shouldn't be git tracked
- Dialog boxes at this point are supposed to be purely informative, so as long as they deliver the message and are consistent with the ui design, no need to make them aesthetically pleasing

Next priorities:
- Add redo for transformation history
- Add save as JSON for save feature


### PATCH NOTES: 27/07/2022

Major changes:
- Javadoc documentation for everything
- Break up in smaller packages based on MVC model
  - Graph, Transformations, and Menu
- Add readme file

Minor changes:
- ~~Abstraction~~ Hierarchical level counter now works in correct direction
- Children and edges collections now in hash sets
- Edges represented by custom class


### PATCH NOTES: 05/08/2022

Major changes:
Hashmap that includes all vertices
- Used for calling a specific vertex (no longer does tree search for it)

Minor changes:
- Refactor abstraction and abstraction levels to hierarchy and hierarchical levels (104fe1a0 commit with the
abstraction tutorial chris liked, to be made into its own branch)
- Readme has all features of application
- Package descriptions now concise
- NestedGraphTree object has references to edges which, in turn, have references to the vertices (not the id's!)

Assumptions:
- The tree search is still a method, it's just no one calls it, should be left in to allow for potential tree
search in future? (accepted, method exists but is never used)
