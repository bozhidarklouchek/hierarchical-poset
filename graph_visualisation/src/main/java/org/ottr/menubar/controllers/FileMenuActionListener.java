package org.ottr.menubar.controllers;

import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import org.ottr.App;
import org.ottr.DataLoader;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.models.representation.NestedGraphTree;
import org.ottr.graph.views.MainView;
import org.ottr.graph.views.VisualisationViewerTransformers;

import javax.swing.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Manages the events from the file menu option.
 */
public class FileMenuActionListener
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public FileMenuActionListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    /**
     * Associated with the OPEN menu item option, and once clicked it shows the file browser responsible for opening a
     * new input file with a designated getter in the main view{@link MainView#getOpenFileBrowser()}. The new input may
     * be a serialised version of the model that has been previously saved with the relevant save menu item option, or
     * a JSON file containing graph information.
     * @throws Exception associated with file I/O
     */
    public void openGraphFile() throws Exception
    {
        mainView.getOpenFileBrowser().showDialog(mainView.getFrame(), "Open");
        GraphModel savedGraphModel = null;
        boolean isDataRead = true;

        String pathSelected = mainView.getOpenFileBrowser().getSelectedFile().getAbsolutePath();

        try
        {
            savedGraphModel = DataLoader.initFromSave(pathSelected);
        }
        catch (Exception e)
        {
            System.out.println("Exception when trying to read as serialised file.");
            isDataRead = false;
        }

        // Success in reading serialised file, set new model from it
        if(isDataRead)
        {
            graphModel.setNewModel(savedGraphModel);
        }
        // If not serialised try as JSON
        else
        {
            NestedGraphTree tree = null;
            isDataRead = true;
            try
            {
                // Graph info extracted from JSON
                tree = DataLoader.initFromJSON(pathSelected);
            }
            catch (Exception e)
            {
                System.out.println("Exception when trying to read as JSON file.");
                isDataRead = false;
            }

            // Success reading as JSON, set new model from it
            if(isDataRead)
            {
                // Initiate model
                savedGraphModel = new GraphModel
                        (
                                tree
                        );
                graphModel.setNewModel(savedGraphModel);
            }
        }

        // If data read successful as JSON or serialised stream
        if(isDataRead)
        {
            // Create graph
            Layout<Integer, String> layout = new ISOMLayout<>(graphModel.getCurrentGraph().createGraph());
            mainView.getVisualizationViewer().setGraphLayout(layout);

            // Add vertex and edge transformers
            VisualisationViewerTransformers.addTransformers(mainView.getVisualizationViewer(), graphModel.getCurrentGraph(), graphModel.getMergedVertices());

            // If just opened nothing selected
            mainView.getRightSideScreen().setVertexIdLabel("No vertex selected yet.");
            mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
            mainView.getRightSideScreen().getGoDownButton().setVisible(false);
            mainView.getRightSideScreen().getMergeButton().setVisible(false);
            mainView.getRightSideScreen().getGoUpButton().setVisible(false);

            mainView.drawGUI(graphModel.getCurrentGraph());
        }
        else
        {
            System.out.println("Failed to read file. Must be JSON or serialised stream!");
        }
    }

    /**
     * Associated with the SAVE menu item option, and once clicked it saves the current state of te internal graph
     * structure to the default save directory from {@link App#getSavePath()}.
     */
    public void saveWorkingGraphToSaveDir() throws IOException
    {
        FileOutputStream outputFile = new FileOutputStream(App.getSavePath());
        ObjectOutputStream outputObj = new ObjectOutputStream(outputFile);
        outputObj.writeObject(graphModel);
        outputObj.flush();
        outputObj.close();
    }

    /**
     * Associated with the SAVE AS menu item option, and once clicked it shows the file browser responsible for saving a
     * the current state of te internal graph structure with a designated getter in the main view
     * {@link MainView#getSaveAsFileBrowser()}. The current state is serialised and saved at the specified directory
     * from the file browser.
     */
    public void saveWorkingGraphToSpecifiedDir() throws IOException {
        mainView.getSaveAsFileBrowser().showDialog(mainView.getFrame(), "Save as");
        mainView.getSaveAsFileBrowser().setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        FileOutputStream outputFile = new FileOutputStream(mainView.getSaveAsFileBrowser().getSelectedFile().getAbsolutePath());
        ObjectOutputStream outputObj = new ObjectOutputStream(outputFile);
        outputObj.writeObject(graphModel);
        outputObj.flush();
        outputObj.close();

    }
}
