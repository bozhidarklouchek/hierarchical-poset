package org.ottr.graph.models;

import org.ottr.graph.controllers.listeners.NestedGraphEdgeSelectionListener;
import org.ottr.graph.controllers.listeners.NestedGraphVertexSelectionListener;
import org.ottr.graph.models.representation.NestedGraphTree;
import org.ottr.menubar.models.MenuMode;
import org.ottr.transformations.models.TransformationHistory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Holds all state information for the application.
 */
public class GraphModel implements Serializable
{
    private HashMap<Integer, NestedGraphTree> allVertices;
    private NestedGraphTree root;
    private NestedGraphTree currentGraph;
    private NestedGraphTree currentPreviewGraph;
    private TransformationHistory transformationHistory;
    private ArrayList<NestedGraphTree> mergedVertices;
    private MenuMode mode;
    private boolean protectVertices = false;
    private boolean protectEdges = false;
    private int vertexCounter;

    /**
     * Class constructor for creating a model only from a NestedGraphTree object, which would represent the entire
     * graph structure internally. This constructor is typically called when reading a JSON object as input and the
     * model is created from the subsequently read information.
     * @param tree the internally represented graph structure to be used in the application
     * @see org.ottr.DataLoader#initFromJSON(String)
     */
    public GraphModel
            (
                    NestedGraphTree tree
            )
    {
        // The root always looks at the first nested graph tree, so it can easily be referenced for other operations
        this.root = tree;

        // At the beginning, the current graph to be displayed will be the root
        this.currentGraph = tree;

        // All other attributes are initially empty or have a relevant default value
        transformationHistory = new TransformationHistory();
        mergedVertices = new ArrayList<>();
        mode = new MenuMode();
        vertexCounter = 0;
    }

    /**
     * Substitutes the current model for a new model, usually loaded from a saved file. It's effectively a new model,
     * but with keep the same reference by only changing all the attributes. Usually done when loading a safe file
     * @param newModel the new model that contains all new information to be used for the current model
     * @see org.ottr.DataLoader#initFromSave(String)
     */
    public void setNewModel(GraphModel newModel)
    {

        this.allVertices = newModel.allVertices;
        this.root = newModel.root;

        // When loading a new model the user will start exploring from the root
        this.currentGraph = this.root;

        // Update all the attributes of the model with the attributes of the new model
        this.transformationHistory = newModel.transformationHistory;
        this.mergedVertices = newModel.mergedVertices;
        this.mode = newModel.mode;
        this.vertexCounter = newModel.vertexCounter;
        this.protectVertices = newModel.protectVertices;
        this.protectEdges = newModel.protectEdges;
    }

    /**
     * Getter for hashmap of all vertices, necessary to transfer user to graphs which are no longer on screen.
     * For example, let's say a user has performed a transformation on a high hierarchical level, if they now descend
     * deeper into the levels and wish to undo it, the hashmap is used to retrieve the first object the transformation
     * was performed on.
     * @return hashmap of all vertices
     */
    public HashMap<Integer, NestedGraphTree> getAllVertices()
    {
        return allVertices;
    }

    public void setAllVertices(HashMap<Integer, NestedGraphTree> allVertices)
    {
        this.allVertices = allVertices;
    }

    /**
     * Getter for the root of the graph structure that's being used in the application. Can be used ina number of
     * operations like counting how many vertices are in the graph structure, counting hierarchical levels, etc.
     * @return the root of the internally represented graph structure
     * @see NestedGraphTree#countVerticesInTree()
     * @see NestedGraphTree#getHierarchicalLevel()
     */
    public NestedGraphTree getRoot()
    {
        return root;
    }

    /**
     * Getter for the current graph that should be displayed onscreen. Usually used to get some relevant graph
     * information.
     * @return the current graph to be displayed on screen
     */
    public NestedGraphTree getCurrentGraph()
    {
        return currentGraph;
    }

    /**
     * Setter for the current graph, usually called after an operation that would change the visualisation viewer has
     * taken place, (navigating a hierarchical level, transformation, etc.) because that would change the graph in
     * some way.
     * @param currentGraph new current graph
     */
    public void setCurrentGraph(NestedGraphTree currentGraph)
    {
        this.currentGraph = currentGraph;
    }

    /**
     * Getter for the current preview graph, usually called to display the preview visualisation viewer only in preview
     * mode in {@link org.ottr.menubar.models.MenuMode}.
     * @return the current preview graph
     */
    public NestedGraphTree getCurrentPreviewGraph()
    {
        return currentPreviewGraph;
    }

    /**
     * Setter for the current preview graph, usually called when the preview graph has been changed only in preview
     * mode in {@link org.ottr.menubar.models.MenuMode}.
     * @param currentPreviewGraph the new current preview graph
     */
    public void setCurrentPreviewGraph(NestedGraphTree currentPreviewGraph)
    {
        this.currentPreviewGraph = currentPreviewGraph;
    }

    /**
     * Getter for the transformation history object, usually done to gauge some relevant information about it, for
     * example if it's empty or to peek at the last transformation.
     * @return the transformation history of the graph structure
     */
    public TransformationHistory getTransformationHistory()
    {
        return transformationHistory;
    }

    /**
     * Getter for the list of merged vertices, usually called in contexts where the number in the list of a merged
     * vertex is relevant, like when the label of a merged vertex is being determined in
     * {@link org.ottr.graph.views.VisualisationViewerTransformers#addTransformers}
     * @return list of merged vertices
     */
    public ArrayList<NestedGraphTree> getMergedVertices()
    {
        return mergedVertices;
    }

    /**
     * Getter for the current menu mode object, which holds information about which mode the application is in,
     * {@link MenuMode.mode}
     * @return menu mode object representing the menu mode state of the application
     */
    public MenuMode getMode()
    {
        return mode;
    }

    /**
     * Getter for the boolean flag that is important to the selection logic of the application. Usually considered with
     * the {@link GraphModel#isProtectEdges()}. In the application a user should <b>not</b> be able to select both a
     * vertex and an edge, for specific information and examples see
     * {@link NestedGraphVertexSelectionListener#selectVertex()} and
     * {@link NestedGraphEdgeSelectionListener#selectEdge()}
     * @return boolean to protect vertices from new selection events
     */
    public boolean isProtectVertices()
    {
        return protectVertices;
    }

    /**
     * Setter for the boolean flag that is important to the selection logic of the application. Usually considered with
     * the {@link GraphModel#setProtectEdges(boolean)}. In the application a user should <b>not</b> be able to select
     * both a vertex and an edge, for specific information and examples see
     * {@link NestedGraphVertexSelectionListener#selectVertex()} and
     * {@link NestedGraphEdgeSelectionListener#selectEdge()}
     * @param protectVertices new boolean to set if vertices should be protected from new selection events
     */
    public void setProtectVertices(boolean protectVertices)
    {
        this.protectVertices = protectVertices;
    }

    /**
     * Getter for the boolean flag that is important to the selection logic of the application. Usually considered with
     * the {@link GraphModel#isProtectVertices()}. In the application a user should <b>not</b> be able to select both a
     * vertex and an edge, for specific information and examples see
     * {@link NestedGraphVertexSelectionListener#selectVertex()} and
     * {@link NestedGraphEdgeSelectionListener#selectEdge()}
     * @return boolean to protect edges from new selection events
     */
    public boolean isProtectEdges()
    {
        return protectEdges;
    }

    /**
     * Setter for the boolean flag that is important to the selection logic of the application. Usually considered with
     * the {@link GraphModel#setProtectVertices(boolean)}. In the application a user should <b>not</b> be able to select
     * both a vertex and an edge, for specific information and examples see
     * {@link NestedGraphVertexSelectionListener#selectVertex()} and
     * {@link NestedGraphEdgeSelectionListener#selectEdge()}
     * @param protectEdges new boolean to set if edges should be protected from new selection events
     */
    public void setProtectEdges(boolean protectEdges)
    {
        this.protectEdges = protectEdges;
    }

    /**
     * Getter for the vertex counter that represents an accurate count of all vertices from the root inclusive, usually
     * called to determine the next available vertex id for when assigning id to new vertices, for example merged ones.
     * @return vertex count of graph structure
     * @see NestedGraphTree#countVerticesInTree()
     */
    public int getVertexCounter()
    {
        return vertexCounter;
    }

    /**
     * Setter for the vertex counter that represents an accurate count of all vertices from the root inclusive, usually
     * called to update the next available vertex id after assigning id to a new vertex, for example a merged one.
     * @param vertexCounter new vertex count of graph structure
     * @see NestedGraphTree#countVerticesInTree()
     */
    public void setVertexCounter(int vertexCounter)
    {
        this.vertexCounter = vertexCounter;
    }

    /**
     * Increments the vertex counter that represents an accurate count of all vertices from the root inclusive, usually
     * called to increment by one after assigning id to a new vertex, for example a merged one.
     * @param newVerticesCounter new vertex count of graph structure
     * @see NestedGraphTree#countVerticesInTree()
     */
    public void incrementVertexCounterWith(int newVerticesCounter)
    {
        this.vertexCounter += newVerticesCounter;
    }
}