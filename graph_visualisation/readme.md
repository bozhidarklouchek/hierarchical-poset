# Graph Visualisation Tool
This tool can be used to visualise nested graph structures that are specified in a JSON format. 
The tool itself is built using Java for the backend logic and Java Swing for the GUI.

## FAQ

### What is graph hierarchical abstraction in the application?
A nested graph structure represents a tree with vertices that can hold graphs within themselves. An edge in
the tree between two vertices means that the destination vertex is present within the graph of the start
vertex.

To get a better understanding, imagine we have a normal directed graph of 200 vertices each of which
containing a number, from 1 to 200 with random directed edges between them. The resulting graph can be represented as
such:

![img_1.png](resources/written_demo_pics/200_unorganised.png)

However, we can see how quickly everything has gotten way too complicated! A user won't be able to navigate
through this in an easy way. In order to simplify this graph we can apply **hierarchical abstraction**, by
selecting a finite number of groups containing a finite number of vertices and grouping those groups together into
**deep vertices**. A deep vertex just means that it has a graph within itself im which there are more vertices!
Let's do this on our example graph by having 10 groups such that vertices: 1-20, 21-40,
... , 181-200 are in groups 1, 2, ..., 10. We see this here:

![img_2.png](resources/written_demo_pics/200_organised.png)

This is still fairly complicated, and we wouldn't want the user to group vertices each time, so we substitute the
groups for **deep vertices**. And so:

![img_3.png](resources/written_demo_pics/200_abstracted.png)

We clearly see how the graph is now simplified. Please note, this example is just to introduce the idea of 
applying an abstraction hierarchy to vertices, not to be confused as to how the actual nested graph structure works. 

To conclude, hierarchical abstraction in the application works by creating a hierarchy within the graph itself. In the
given example above we originally had 200 shallow vertices, that is, vertices which have no nested graphs within
themselves, and later on we created a new hierarchical level by adding a layer of deep vertices above them. Think
of it like a pyramid! After we applied the structure we are looking at 10 deep vertices, or, the highest
hierarchical level, if we want to go down a level, we could, and the graph we see will be determined by which deep
vertex we explore.

### What are hierarchical levels?

Now that we understand the concept of hierarchical abstraction, we can introduce the concept of **hierarchical levels**.
Let's take a simpler example, a graph of only 10 vertices as such:

![img_4.png](resources/written_demo_pics/3_graphs.png)

This graph structure has no hierarchical levels. Why? Because it has no deep vertices, but only shallow vertices,
the vertices in the illustrated graph have no nested graphs within themselves. Now let's add three deep vertices
to represent the three distinct disjoint groups, so we will have:
1. Deep vertex 11 for 1,2,3,4
2. Deep vertex 12 for 5,6,7
3. Deep vertex 13 for 8,9,10

And so we have added a hierarchical layer and have resulted in the following graph:

![img_5.png](resources/written_demo_pics/3_abstracted_graphs.png)

The shallow vertices 1-10 are represented in the corresponding nested graphs within deep vertices 11, 12, and 13.
So on the inside of them we can see the graphs we saw a second ago:

![ps.png](resources/written_demo_pics/abstraction_example.png) 

So now we have one level of hierarchy, and this concept can go even further by considering that a deep 
vertex can contain another deep vertex. When talking about a graph, it's important to specify the vertex which contains
that graph. We say that:
- The graph with the deep vertices is nested in the root vertex, and so that graph and the root vertex have a
hierarchical level of 1 because it's 'one step' away from the bottom of the hierarchy, and
- the three graphs showing the shallow vertices are nested in the corresponding deep vertex representing them, and so
all three of these graphs and those tree deep vertices have a hierarchical level of 0, since that is the bottom.

Here is a good place to make a very important point about the application. In order to see a graph,
it needs a vertex in which to go into and grab its nested graph, so when a user sees the graph with vertices
1-4, they see this graph that's **nested** inside vertex 11. **So when talking about a graph we must always specify a
vertex that's associated with it.** This is also true for the initial graph of vertices 11, 12, and 13 - they are stored
in a graph that's being nested in vertex 0 (root), a vertex that must always be specified in the input JSON file.
Here is a visual representation of the internal graph tree structure and the hierarchical levels in it:

![aaa.png](resources/written_demo_pics/hierarchical_levels.png)

The reason why the bottom row doesn't have a hierarchical level is because the notion of a hierarchical level
can only exist when referring to a deep vertex. The bottom has no vertices which have graphs within themselves, so it's
impossible for them to have a hierarchical level. This, again, might be a bit odd to think about - but you can
just remember that a hierarchical level is only relevant to a deep vertex. In the given example it seems odd that
the last row has no hierarchical level, but the vertices on that bottom row (1-10) are all vertices inside
graphs of the last hierarchical level (11-13). So in the application you can only go as down as that second to last row
and see the corresponding graph (1-4 for 11, 5-7 for 12, 8-10 for 13). So you could not traverse lower than that.

### Is it possible for a graph to contain only shallow vertices but be on a hierarchical level other than zero?

Yes! However, if that's an issue for your use case, it's something you must look into.

As established, a hierarchical level is only relevant to a deep vertex and its nested graph, but the application can
handle a case like this:

![aaa.png](resources/written_demo_pics/edge_case.png)

Notice this doesn't conflict with the rules we've established! We can see shallow vertices 1-4 are located on
hierarchical level 0 now, but that doesn't mean that they now have a hierarchical level! They have no graphs and are
shallow, so by definition they may not have a hierarchical level associated with themselves. They are on the same bar
as 14 and 15 just so it's clear that they are children of 11.

### How do I give input to the application?

Input is given in the JSON format. For example, in order to specify the graph that's given as an
example to the above question "What are hierarchical levels?", that is a graph with three deep
vertices 11, 12, and 13, that hold the graphs containing vertices 1-4, 5-7, and 8-10, write this in the
JSON input file:

`{`
`"0":
{
"vertices": [11,12,13],
"edges": [],
"information_object":{}
}`
`"1":{"vertices": [],"edges": [],"information_object": {}},`
`"2":{"vertices": [],"edges": [],"information_object": {}},`
`"3":{"vertices": [],"edges": [],"information_object": {}},`
`"4":{"vertices": [],"edges": [],"information_object": {}},`
`"5":{"vertices": [],"edges": [],"information_object": {}},`
`"6":{"vertices": [],"edges": [],"information_object": {}},`
`"7":{"vertices": [],"edges": [],"information_object": {}},`
`"8":{"vertices": [],"edges": [],"information_object": {}},`
`"9":{"vertices": [],"edges": [],"information_object": {}},`
`"10":{"vertices": [],"edges": [],"information_object": {}},`
`"11":{"vertices": [1,2,3,4],"edges": [[1,2],[1,3],[1,4]],"information_object": {}},`
`"12":{"vertices": [5,6,7],"edges": [[5,6],[5,7]],"information_object": {}},`
`"13":{"vertices": [8,9,10],"edges": [[8,9],[8,10]],"information_object": {}}`
`}`

Several things to note here:
1. **Always** supply a vertex with id 0 as your first vertex in the JSON file that holds all the vertices
you wish to display on the initial screen. This will be the root. (In essence, vertex 0
is the ultimate deep vertex, as it holds all other vertices.)
2. Supply all vertices with the graphs within them and their information objects as the given key-value pairs:
   1. **_vertices_**: a list of the id's of vertices that are present in the graph within the current vertex (empty if 
   vertex is a shallow vertex, non-empty otherwise).
   2. **_edges_**: a list of lists (pairs) of starting vertex id and ending vertex id of the edges that are present in
   the graph within the current vertex (empty if vertex is a shallow vertex, non-empty otherwise).
   3. **_information_object_**: JSON object with key-value pairs relevant to your use case.

### How do I run the application?

You may build the project using Maven. The application has a main class specified in the manifest called App.

There is more information in the pom.xml file specified for the building process, including the maven jar
plugin and the path to the main class.

## Application features

### Traversing hierarchical levels

A user can either go into a new hierarchical level (go deeper into them) or come up from one. We'll examine both.

#### Go down a hierarchical level

To go down a hierarchical level two conditions must be met:
1. The application is in PICKING mode, and
2. The user has only ONE deep vertex selected.

If both are true then a button to go down an hierarchical level through the deep vertex.

![img_6.png](resources/written_demo_pics/selected_abstracted.png)

When the user clicks the designated button the application will traverse one level below:

![img_7.png](resources/written_demo_pics/down_one_level.png)

So essentially, this happened:

![bbbb.png](resources/written_demo_pics/go_down_level.png)

The user can go down a hierarchical level either by clicking the button as demonstrated or by double-clicking the left
mouse button on a deep vertex.

#### Go up a hierarchical level

To go up a hierarchical level two conditions must be met:
1. The application is in PICKING mode, and
2. The current graph isn't represented by the root vertex (with id 0).

If both are true then a button to go up from the hierarchical level through the parent of the current vertex
that's holding the graph we're looking at.

![img_7.png](resources/written_demo_pics/down_one_level.png)

When the user clicks the designated button the application will traverse one level above.

![img.png](resources/written_demo_pics/went_up_level.png)

So essentially, this happened:

![1.png](resources/written_demo_pics/go_up_level.png)

The user can go up several hierarchical levels by holding down the CTRL key and rolling the
mouse wheel backwards.

### Menu options

#### File menu option

With the file menu option a user can save state of their current work. This is done by serialising the 
main model of the application, which can later be deserialised so work can be continued.

There are currently three items:
1. Open - open either a JSON input file of a graph or a serialised model saved from earlier
2. Save - serialise the main model and save the file to the default save directory
3. Save as - serialise the main model and save the file to a specific directory

#### Mode menu option

With the mode menu option a user can change the mode the application is in, these modes can be
used to traverse hierarchical levels, explore a graph,  or preview hierarchical levels.

There are currently three items:
1. **Picking**
   1. can select vertices/edges (left-click, shift left-click, and drag left-click)
   2. pressing ctrl and left-click on a vertex centers it on the screen
   3. can traverse hierarchical levels and merge vertices
2. **Transforming**
   1. can perform geometrical transformations:
      1. translate - drag left-click
      2. rotate - shift left-click and drag
      3. shear - ctrl left-click and drag
3. **Previewing**
   1. can preview graphs within deep vertices by right-clicking on relevant deep vertex

### Transformations

#### Merging

To merge more than one vertex in the same graph
1. The application is in PICKING mode, and
2. The user has selected more than one vertex

If both are true then a button to merge the vertices will appear:

![img.png](resources/written_demo_pics/merge_screen.png)

When the user clicks the designated button the vertices will merge, resulting in a new vertex,
that holds all information that the previous two held, combining their children, edges, and information
objects:

![img.png](resources/written_demo_pics/after_merge.png)

So essentially, this happened:

![img.png](resources/written_demo_pics/result_of_merge.png)

It's important to note that the new merged vertex now has the same parent as the vertices that were
merged to create it. The merge then affects the selected vertices to be merged, their parent, and their
children - as all references must be updated to reflect the merge.

#### Undoing Transformations

After having performed a transformation on the graph, for example a merge, a user can undo it by holding down the
control button and then pressing the Z key (ctrl+z). There are some complications, however, as a user can undo a
transformation on a graph that's currently NOT on screen. There are two general cases when it comes to undoing
transformations:

**1. The user is on the same screen as the transformation they want to undo.**

That's simple enough, as the graph onscreen will just revert to its previous state. Consider this case,
where vertices 1 and 2 have been merged into vertex M1. After undoing we revert to the original
state.

**BEFORE MERGE**

Graph nested in vertex 0, holding vertices 1 and 2:
![img_1.png](resources/written_demo_pics/img_1.png)
Graph nested in vertex 1, holding vertices 3-9:
![img_3.png](resources/written_demo_pics/img_3.png)
Graph nested in vertex 2, holding vertices 20-22:
![img_4.png](resources/written_demo_pics/img_4.png)

So now we will merge vertices 1 and 2, and subsequently that will affect the graphs nested
in them.

**AFTER MERGE**

Graph nested in vertex 0, holding vertex M1, the result of merging 1 and 2:
![img_2.png](resources/written_demo_pics/img_2.png)
Graph nested in vertex M1, holding vertices 3-9 and 20-22:
![img_5.png](resources/written_demo_pics/img_5.png)

So then the second graph is a **result of the merge** because it's nested in a merged vertex. It exists only
because the vertices on that hierarchical level were merged to create it. If we now go back to this screen:
![img_2.png](resources/written_demo_pics/img_2.png)
After undoing:
![img_1.png](resources/written_demo_pics/img_1.png)
And the old graph with vertices 1-2 is back! This was simple enough, as we were on the same screen as the
transformation, what happens if we're not on it?

**1. The user is NOT on the same screen as the transformation they want to undo.**

If a user tries to undo a transformation but is not on the viewer isn't visualising the relevant graph, then this popup
will appear:

![img_6.png](resources/written_demo_pics/img_6.png)

The user can either:
- be transferred to the graph where the original transformation happened and have it undone
- not move away from the current, but undo the last transformation anyway
- cancel

The first option is basically the same as being on the relevant graph in the first place, just the application
moves you automatically. The second option is much more interesting, as it does everything behind the scenes. There
is one exception, however - if a user is on a graph that's the result of a transformation, and they try to undo the
transformation that created the graph, they risk destroying the graph they're looking at right now! Let's see an
example:

First, we begin from the root graph, where there are vertices 1 and 2:
![img_1.png](resources/written_demo_pics/img_1.png)
We know both of them have graphs nested inside each, the first one with vertices 3-9 and the second one with 20-22, we
merge 1 and 2 to get M1:
![img_2.png](resources/written_demo_pics/img_2.png)
And now we traverse one level down to the merged graph with vertices 3-9 and 20-22:
![img_5.png](resources/written_demo_pics/img_5.png)
We now try to undo the merge we just did, and so we get the popup:
![img_6.png](resources/written_demo_pics/img_6.png)
And finally, we try to not change our graph but still undo, so the middle option. This prompts a secondary popup:
![img_7.png](resources/written_demo_pics/img_7.png)

The application has recognised that the graph the user is on will cease to exist if the transformation is undone, so it
forces the user to either go to that graph or cancel. Both options will yield the same outcome as having chosen the same
things in the first popup.

## Things that can be improved/added!

### UI and design
The application has been built originally without serious consideration for aesthetics or many
UI principles, making the look and feel more appealing, making sure all buttons/screens and other elements
are consistent is important.

### Transformations
The first transformation was the merging operation as it's the most general one, more transformations
might be necessary for different use cases.

Making sure the transformation history is functional (being able to undo AND redo) and consistent with all
transformations.

### General
An updated configuration setting file that can directly influence tha application will be very useful. 

Making sure there is an updated help menu item with info about controls of the application would be 
helpful for new users.

It would be very good to be able to have multiple windows of the application running, all of which
are synchronised - so if a transformation is performed on one then it will appear on the other one as well.

### Project Architecture
The project has a specific architecture, split into major packages and then into MVC patterns. This is 
done mainly to allow developers to jump in and start exploring much easier, but there are a lot of things that can be
improved in this sense as well, lots of view elements in the main graph view package might become big enough to
deserve their own package, also it might be good to split the entire project into UI and logic, depending on its 
development.