# The graph package

This is the package responsible for representing the graph logic inside the application, including its visual
elements.

Everything graph will be found here! From changing vertices/edges visually to changing their mouse/key/button events.

Here is also the main model of the application, which gets serialised and deserialise, as it contains all information
necessary to re-create the graph state.