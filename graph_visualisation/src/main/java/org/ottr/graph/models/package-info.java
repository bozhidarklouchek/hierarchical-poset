/**
 * Representing the internal graph structure and state information.
 */
package org.ottr.graph.models;