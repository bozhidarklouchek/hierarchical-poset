package org.ottr.menubar.views;

import org.ottr.menubar.controllers.FileMenuActionListener;
import org.ottr.menubar.controllers.ModeMenuActionListener;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
/**
 * Holds all view elements of the menu for the application.
 */
public class GraphMenu
{
    private final JMenuBar menuBar;

    // File menu
    private final JMenuItem fileMenuOpen;
    private final JMenuItem fileMenuSave;
    private final JMenuItem fileMenuSaveAs;

    // Mode menu
    private final JRadioButtonMenuItem modeMenuPicking;
    private final JRadioButtonMenuItem modeMenuTransforming;
    private final JRadioButtonMenuItem modeMenuPreviewing;

    /**
     * Class constructor.
     */
    public GraphMenu()
    {
        // General menu information
        menuBar = new JMenuBar();
        Font menuOptionsFont = (new Font("SansSerif", Font.BOLD, 12));
        Font menuOptionsItemsFont = (new Font("SansSerif", Font.PLAIN, 11));
        UIManager.put("Menu.font", menuOptionsFont);
        UIManager.put("MenuItem.font", menuOptionsItemsFont);
        UIManager.put("RadioButtonMenuItem.font", menuOptionsItemsFont);


        // File menu option
        String[] fileMenuOptions = {"Open", "Save", "Save as"};
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        fileMenu.getAccessibleContext().setAccessibleDescription("Menu with file manipulation options.");

        // Open new file
        fileMenuOpen = new JMenuItem(fileMenuOptions[0]);

        // Save current file, overwriting previous version
        fileMenuSave = new JMenuItem(fileMenuOptions[1]);

        // Save current version as new file
        fileMenuSaveAs = new JMenuItem(fileMenuOptions[2]);

        // Add to file menu
        fileMenu.add(fileMenuOpen);
        fileMenu.addSeparator();
        fileMenu.add(fileMenuSave);
        fileMenu.add(fileMenuSaveAs);

        // Add file menu to menu bar
        menuBar.add(fileMenu);


        // Mode menu option
        String[] modeMenuOptions = {"Picking", "Transforming", "Previewing"};
        JMenu modeMenu = new JMenu("Mode");
        modeMenu.setMnemonic(KeyEvent.VK_M);
        modeMenu.getAccessibleContext().setAccessibleDescription("Menu with different graph visualisation modes.");

        ButtonGroup modeMenuItemsGroup = new ButtonGroup();

        // Picking mode
        modeMenuPicking = new JRadioButtonMenuItem(modeMenuOptions[0]);
        modeMenuPicking.setSelected(true);
        modeMenuPicking.setMnemonic(KeyEvent.VK_P);
        modeMenuPicking.setAccelerator
                (
                        KeyStroke.getKeyStroke
                                (KeyEvent.VK_P, InputEvent.ALT_DOWN_MASK)
                );

        // Transforming mode
        modeMenuTransforming = new JRadioButtonMenuItem(modeMenuOptions[1]);
        modeMenuTransforming.setMnemonic(KeyEvent.VK_T);
        modeMenuTransforming.setAccelerator
                (
                        KeyStroke.getKeyStroke
                                (KeyEvent.VK_T, InputEvent.ALT_DOWN_MASK)
                );

        // Previewing mode
        modeMenuPreviewing = new JRadioButtonMenuItem(modeMenuOptions[2]);
        modeMenuPreviewing.setMnemonic(KeyEvent.VK_R);
        modeMenuPreviewing.setAccelerator
                (
                        KeyStroke.getKeyStroke
                                (KeyEvent.VK_R, InputEvent.ALT_DOWN_MASK)
                );

        // Add to button group
        modeMenuItemsGroup.add(modeMenuPicking);
        modeMenuItemsGroup.add(modeMenuTransforming);
        modeMenuItemsGroup.add(modeMenuPreviewing);

        // Add to mode menu
        modeMenu.add(modeMenuPicking);
        modeMenu.add(modeMenuTransforming);
        modeMenu.add(modeMenuPreviewing);

        // Add mode menu to menu bar
        menuBar.add(modeMenu);
    }

    /**
     * Getter for the menu bar, usually called to be added to the frame in the initiation of the application.
     * @return the menu bar of the application
     */
    public JMenuBar getMenuBar()
    {
        return menuBar;
    }

    /**
     * Getter for the open menu option from the file menu, usually called to add the event listener.
     * @see FileMenuActionListener#openGraphFile()
     * @return open menu item button
     */
    public JMenuItem getFileMenuOpenButton()
    {
        return fileMenuOpen;
    }

    /**
     * Getter for the save menu option from the file menu, usually called to add the event listener.
     * @see FileMenuActionListener#saveWorkingGraphToSaveDir()
     * @return save menu item button
     */
    public JMenuItem getFileMenuSaveButton()
    {
        return fileMenuSave;
    }
    /**
     * Getter for the save as menu option from the file menu, usually called to add the event listener.
     * @see FileMenuActionListener#saveWorkingGraphToSpecifiedDir()
     * @return save as menu item button
     */
    public JMenuItem getFileMenuSaveAsButton()
    {
        return fileMenuSaveAs;
    }

    /**
     * Getter for the picking menu option from the mode menu, usually called to add the event listener.
     * @see ModeMenuActionListener#selectModePicking()
     * @return picking menu item button
     */
    public JRadioButtonMenuItem getModeMenuPickingRadioButton()
    {
        return modeMenuPicking;
    }

    /**
     * Getter for the transforming menu option from the mode menu, usually called to add the event listener.
     * @see ModeMenuActionListener#selectModeTransforming()
     * @return transforming menu item button
     */
    public JRadioButtonMenuItem getModeMenuTransformingRadioButton()
    {
        return modeMenuTransforming;
    }

    /**
     * Getter for the previewing menu option from the mode menu, usually called to add the event listener.
     * @see ModeMenuActionListener#selectModePreviewing()
     * @return previewing menu item button
     */
    public JRadioButtonMenuItem getModeMenuPreviewingRadioButton()
    {
        return modeMenuPreviewing;
    }
}