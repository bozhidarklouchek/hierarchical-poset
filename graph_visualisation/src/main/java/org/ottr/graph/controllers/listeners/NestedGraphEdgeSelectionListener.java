package org.ottr.graph.controllers.listeners;

import org.ottr.graph.models.GraphModel;
import org.ottr.graph.views.MainView;
import org.ottr.menubar.models.MenuMode;

/**
 * Manages all events from edge selection.
 */
public class NestedGraphEdgeSelectionListener
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public NestedGraphEdgeSelectionListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    /**
     * Gets called each time there is a change within the selected edges (includes a new edge being selected or an edge
     * being unselected). Currently, selection events are as follows:
     * <ul>
     *   <li>If no edges are selected - nothing happens.</li>
     *   <li>If only one edge is selected - option to merge the two vertices between it appears, also known as
     *   "contracting" the edge.</li>
     *   <li>more than one are selected - nothing happnes.</li>
     * </ul>
     */
    public void selectEdge()
    {
        // Array of currently selected edges
        Object[] selected_edges = mainView.getVisualizationViewer().getPickedEdgeState().getSelectedObjects();

        // When an item listener for vertices/edges is called, the program clears the selection list for the other
        // one - so if you call the vertex item listener, the edge list will be cleared (this is done to avoid
        // having both edges and vertices selected at the same time

        // We use the protect flags to save ourselves from falling into a loop that will clear all selections; if we
        //  have 5 edges selected, and then we multi-select 3 vertices, they'll be added to the list one by one, so
        // first we add vertex A, which will trigger the loop to clear all edges, so edge 1 will be cleared, but
        // that will trigger all vertices to be cleared (as that's a change in the edge item listener). Then edge 2,
        // and 3 will be removed. After, vertex B will be added and then C - but A will still be missing!
        if(!graphModel.isProtectVertices());
        {
            graphModel.setProtectEdges(true);
            // Unselect all selected vertices so only either vertices or edges are selected
            Object[] selected_vertices = mainView.getVisualizationViewer().getPickedVertexState().getSelectedObjects();
            for (Object selected_vertex : selected_vertices)
            {
                Integer curr_selected_vertex = (Integer) selected_vertex;
                mainView.getVisualizationViewer().getPickedVertexState().pick(curr_selected_vertex, false);
            }
        }
        graphModel.setProtectEdges(false);

        if(selected_edges.length == 0)
        {
            // Currently, nothing happens when no edges are selected
            mainView.getRightSideScreen().getMergeButton().setVisible(false);
        }
        else if(selected_edges.length == 1)
        {
            //TODO: Add edge selection screen
            mainView.getRightSideScreen().setVertexIdLabel("No vertex selected yet.");
            mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
            mainView.getRightSideScreen().getGoDownButton().setVisible(false);
            if(graphModel.getMode().getChosenMode() != MenuMode.mode.PREVIEWING)
            {
                mainView.getRightSideScreen().getMergeButton().setVisible(true);
            }


        }
        // More than one edge selected
        else
        {
            // Currently, nothing happens when more than one edges are selected
            mainView.getRightSideScreen().getGoDownButton().setVisible(false);
            mainView.getRightSideScreen().getMergeButton().setVisible(false);
        }
    }
}
