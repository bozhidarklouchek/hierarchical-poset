package org.ottr.transformations.models;

import org.ottr.graph.models.representation.Edge;
import org.ottr.graph.models.representation.NestedGraphTree;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Represents the merge between more than one vertex.
 */
public class Merge extends Transformation implements Serializable
{
    @Serial
    private static final long serialVersionUID = 1;
    private final HashSet<NestedGraphTree> oldGraphChildren;
    private final HashSet<Edge> oldGraphEdges;
    private final NestedGraphTree mergedVertex;

    /**
     * Class constructor.
     * The old information about the graph is retained, so it can be undone later on, by just changing the graph's
     * attributes to the saved ones.
     * @param graphId the id of the graph the merge was performed
     * @param oldGraphVertexIds - the old collection of vertex id's on the graph
     * @param oldGraphChildren - the old collection of children on the graph
     * @param oldGraphEdges - the old collection of edges on the graph
     * @param mergedVertex - the new NestedGraphTree object that is the result of merging the releavnt merged vertices
     */
    public Merge
            (
                    int graphId,
                    int hashCode,
                    ArrayList<Integer> oldGraphVertexIds,
                    HashSet<NestedGraphTree> oldGraphChildren,
                    HashSet<Edge> oldGraphEdges,
                    NestedGraphTree mergedVertex
            )
    {
        type = transformationType.MERGE;
        idOfGraphTransformationOccurredOn = graphId;
        hashCodeOfTreeObjTransformationOccurredOn = hashCode;
        this.oldGraphChildren = oldGraphChildren;
        this.oldGraphEdges = oldGraphEdges;
        this.mergedVertex = mergedVertex;
    }

    /**
     * Getter for the collection of children of the graph before the merge took place, usually used to reverse a
     * merge.
     * @return collection of children
     */
    public HashSet<NestedGraphTree> getOldGraphChildren()
    {
        return oldGraphChildren;
    }

    /**
     * Getter for the collection of edges of the graph before the merge took place, usually used to reverse a
     * merge.
     * @return collection of edges
     */
    public HashSet<Edge> getOldGraphEdges()
    {
        return oldGraphEdges;
    }

    /**
     * Getter for the merged vertex object that is the result of merging the relevant vertices.
     * @return merged vertex object
     */
    public NestedGraphTree getMergedVertex() {return mergedVertex;}

    /**
     * Overriden method of the Transformation class, since this is a Merge class, the type is specified as a Merge.
     * @return merge transformation type
     */
    @Override
    public transformationType getType()
    {
        return transformationType.MERGE;
    }

    /**
     * Overriden method of the Transformation class, each transformation has the id of the graph on which it was
     * performed.
     * @return int id of the graph
     */
    @Override
    public int getIdOfGraphTransformationOccurredOn()
    {
        return idOfGraphTransformationOccurredOn;
    }

    /**
     * Getter for the has code the transformation occurred on, often to undo said
     * transformation.
     * @return hash code of object transformation was performed on
     */
    @Override
    public int getHashCodeOfTreeObjTransformationOccurredOn()
    {
        return hashCodeOfTreeObjTransformationOccurredOn;
    }
}
