package org.ottr.graph.models.representation;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ottr.graph.controllers.listeners.NestedGraphButtonListener;

import java.io.Serializable;
import java.util.*;

/**
 * Represents the internal graph structure of the specified graph from the input file.
 */
public class NestedGraphTree implements Serializable
{
    private int id = -1;
    private boolean isRoot = false;
    private boolean isMerged = false;
    private NestedGraphTree parent = null;
    private HashSet<NestedGraphTree> children = new HashSet<>();
    private HashSet<Edge> edges = new HashSet<Edge>();
    private InformationObject informationObject = null;

    /**
     * Class constructor for creating the internal tree graph structure recursively, starting from the root. This is the
     * <b>base case</b> - for example, if we have a graph with root vertex 0, that holds vertices 1 and 2, which hold no
     * vertices of their own, the program will call this constructor once and the call the step case constructor two
     * times - for vertex 1 and vertex 2. These two vertices are considered children of vertex 0, which in turn is
     * considered their parent. The parent also holds information about the edges between vertices 1 and 2. The root
     * <b>MUST</b> have id of 0.
     * @param graphJSON the input JSON file from which the root is extracted
     */
    public NestedGraphTree
    (
        JSONObject graphJSON
    )
    {
        id = 0;
        isRoot = true;
        isMerged = false;
        parent = null;

        // Root must have id of 0
        JSONObject rootVertex = (JSONObject) graphJSON.get(String.valueOf(id));

        // Children in root
        JSONArray verticesInRootJSON = (JSONArray) rootVertex.get("vertices");

        // Convert vertex id's from Long to Integer
        for (Object vertexInRoot : verticesInRootJSON)
        {
            Long longCurrVertexInRoot = (Long) vertexInRoot;
            Integer intCurrVertexInRoot = longCurrVertexInRoot.intValue();

            children.add
                    (
                            new NestedGraphTree
                                    (
                                            intCurrVertexInRoot, //id
                                            this,
                                            graphJSON
                                    )
                    );
        }

        // Edges in root
        JSONArray edgesInRootJSON = (JSONArray) rootVertex.get("edges");
        // Convert edge start and end id's from Long to Integer
        for (Object edgeInRoot : edgesInRootJSON) {
            ArrayList<Long> longCurrEdgeInRoot = (ArrayList<Long>) edgeInRoot;

            Long longCurrEdgeInRootStartId = (Long) longCurrEdgeInRoot.get(0);
            Long longCurrEdgeInRootEndId = (Long) longCurrEdgeInRoot.get(1);

            Integer intCurrEdgeInRootStartId = longCurrEdgeInRootStartId.intValue();
            Integer intCurrEdgeInRootEndId = longCurrEdgeInRootEndId.intValue();

            // The children would have already been set for the parent by the time this code is reached, because all
            // recursive calls would have been made. Then we can simply look in the children collection to add the
            // relevant children in the edge
            Edge intCurrEdgeInRoot = new Edge(getChildWithId(intCurrEdgeInRootStartId),
                    getChildWithId(intCurrEdgeInRootEndId));
            edges.add(intCurrEdgeInRoot);
        }

        // Information object in root
        JSONObject informationObjectJSON = (JSONObject) rootVertex.get("information_object");
        informationObject = new InformationObject(informationObjectJSON);

    }

    /**
     * Class constructor for creating the internal tree graph structure recursively. This is the * <b>step case</b>, in
     * which the root would have already been created and all its subsequent children will follow. Refer to the base#
     * case constructor for an example of how this happens.
     * @param id - the id to be given to the current child
     * @param parent - the {@link NestedGraphTree} object which is the parent of the current vertex
     * @param graphJSON - the input file from which additional information about the vertex is extracted
     */
    public NestedGraphTree
    (
        int id,
        NestedGraphTree parent,
        JSONObject graphJSON
    )
    {
        this.id = id;

        // Since this is the step case of the recursive call there is no way for the vertices here to be roots, and
        // since it's only called in the beginning to create an internal graph structure there cannot be merged
        // vertices.
        this.isRoot = false;
        this.isMerged = false;

        this.parent = parent;

        // Current vertex id
        JSONObject currNestedGraphVertex = (JSONObject) graphJSON.get(String.valueOf(this.id));

        // Children in child
        JSONArray verticesInChildJSON = (JSONArray) currNestedGraphVertex.get("vertices");

        // Convert vertex id's from Long to Integer
        for (Object vertexInChild : verticesInChildJSON)
        {
            Long longCurrVertexInChild = (Long) vertexInChild;
            Integer intCurrVertexInChild = longCurrVertexInChild.intValue();

            children.add
                    (
                            new NestedGraphTree
                                    (
                                            intCurrVertexInChild, //id
                                            this,
                                            graphJSON
                                    )
                    );
        }

        // Edges in child
        JSONArray edgesInChildJSON = (JSONArray) currNestedGraphVertex.get("edges");
        // Convert edge start and end id's from Long to Integer
        for (Object edgeInChild : edgesInChildJSON) {
            ArrayList<Long> longCurrEdgeInChild = (ArrayList<Long>) edgeInChild;

            Long longCurrEdgeInChildStartId = (Long) longCurrEdgeInChild.get(0);
            Long longCurrEdgeInChildEndId = (Long) longCurrEdgeInChild.get(1);

            int intCurrEdgeInChildStartId = longCurrEdgeInChildStartId.intValue();
            int intCurrEdgeInChildEndId = longCurrEdgeInChildEndId.intValue();

            // The children would have already been set for the current vertex by the time this code is reached,
            // because all recursive calls would have been made. Then we can simply look in the children collection
            // to add the relevant children in the edge
            Edge intCurrEdgeInRoot = new Edge(getChildWithId(intCurrEdgeInChildStartId),
                    getChildWithId(intCurrEdgeInChildEndId));
            edges.add(intCurrEdgeInRoot);
        }

        // Information object in child
        JSONObject informationObjectJSON = (JSONObject) currNestedGraphVertex.get("information_object");
        informationObject = new InformationObject(informationObjectJSON);
    }

    /**
     * Class constructor for creating an object given specific attributed, this is done after the initial internal
     * graph structure is created and usually is called after a transformation (e.g. a merge) has taken place. The
     * reason is that the new object would need to be put in the internal structure in the correct place (so having the
     * correct pointers to a parent and children), effectively replacing another vertex.
     * @param id - if of the new vertex
     * @param isRoot - boolean flag stating if the vertex is a root
     * @param isMerged - boolean flag stating if it's merged
     * @param parent - parent reference
     * @param children - collection of children references
     * @param edges - collection of edges between children vertices
     * @param informationObject - information object of vertex, representing additional information and typically
     *                          constructed using the {@link InformationObject}'s recursive constructor if this method
     *                          is called as a result of a merge
     */
    public NestedGraphTree
            (
                    int id,
                    boolean isRoot,
                    boolean isMerged,
                    NestedGraphTree parent,
                    HashSet<NestedGraphTree> children,
                    HashSet<Edge> edges,
                    InformationObject informationObject
            )
    {
        this.id = id;
        this.isRoot = isRoot;
        this.isMerged = isMerged;
        this.parent = parent;
        this.children = children;
        this.edges = edges;
        this.informationObject = informationObject;
    }

    /**
     * Getter for the id, could be for a multitude of reasons, one of which to apply transformers to the specified
     * vertex.
     * @return integer id of vertex
     * @see org.ottr.graph.views.VisualisationViewerTransformers
     */
    public int getId()
    {
        return id;
    }

    /**
     * Checks if the specified vertex is a root or not, usually called to check if the viewer should display the option
     * to go up a hierarchical level (and if it <b>is</b> a root, you could not and should not be able to go up a
     * hierarchical level, since the root is at the highest possible hierarchical level).
     * @return boolean stating if the vertex is a root or not
     */
    public boolean isRoot()
    {
        return isRoot;
    }

    /**
     * Checks if the specified vertex is the result of a merge or not, usually called to properly colour vertex on
     * visualisation viewer (merged vertices are coloured in a different way as to distinguish them from non-merged
     * vertices).
     * @return boolean stating if the vertex is a merged vertex or not
     * @see org.ottr.graph.views.VisualisationViewerTransformers
     * @see NestedGraphButtonListener#merge()
     */
    public boolean isMerged()
    {
        return isMerged;
    }

    /**
     * Getter for the collection of children id's, meaning specifically the id's of the children of the current vertex.
     * @return collection of children id's
     */
    public ArrayList<Integer> getChildrenIds()
    {
        ArrayList<Integer> childrenIds = new ArrayList<>();
        for(NestedGraphTree child : children)
        {
            childrenIds.add(child.getId());
        }
        return childrenIds;
    }

    /**
     * Getter for the collection of children of current node, usually done to check properties about the children.
     * @return collection of children
     */
    public HashSet<NestedGraphTree> getChildren()
    {
        return children;
    }

    /**
     * Gets a specific child of the vertex given the child's id, usually done to progress down a hierarchical level,
     * for example, if the current graph onscreen is showing the graphs inside vertex 3, and the graph contains vertices
     * 4, 5, and 6, then vertices 4, 5, and 6 are vertex 3's children. Then you can call this method on the object
     * representing vertex 3 with a specified child id and obtain that child vertex object.
     * @param childId id of the child you wish to obtain
     * @return child NestedGraphTree object of current NestedGraphTree object
     */
    public NestedGraphTree getChildWithId(int childId)
    {

        for(NestedGraphTree child : children)
        {
            if(child.getId() == childId)
            {
                return child;
            }
        }
        // If no such child
        return null;
    }

    /**
     * Gets a specific vertex object from the internal graph structure given its id by performing a breadth first
     * search from the root. Usually this is called when a transformation was done on a specific graph with id X, and
     * after changing the current graph it's needed again, so if the method is called on the root with X, we will obtain
     * the graph.
     * @param id - the id of the NestedGraphTree object containing the desired graph
     * @return desired NestedGraphTree object
     */
    public NestedGraphTree getGraphWithId(int id)
    {
        Queue<NestedGraphTree> queue = new LinkedList<>();

        queue.offer(this);

        while(!queue.isEmpty())
        {
            NestedGraphTree currVertex = queue.remove();
            if(currVertex.getId() == id)
            {
                return currVertex;
            }

            HashSet<NestedGraphTree> currChildren = currVertex.getChildren();
            for(NestedGraphTree currChild : currChildren)
            {
                queue.offer(currChild);
            }
        }
        return null;
    }

    /**
     * Getter for the parent NestedGraphTree object of the NestedGraphTree object the method is called on, usually done
     * to go up a level of hierarchy.
     * @return parent NestedGraphTree object
     */
    public NestedGraphTree getParent()
    {
        return parent;
    }

    /**
     * Getter for the edges in the graph in the NestedGraphTree object the method is called on.
     * @return edges of nested graph within NestedGraphTree object
     */
    public HashSet<Edge> getEdges()
    {
        HashSet<Edge> edgesToReturn = new HashSet<>();
        for(Edge currEdge : edges)
        {
            Edge currEdgeToReturn = new Edge(currEdge.getStartVertex(), currEdge.getDestinationVertex());
            edgesToReturn.add(currEdgeToReturn);
        }
        return edgesToReturn;
    }

    /**
     * Getter for the hierarchical level of the NestedGraphTree object the method is called on. The hierarchical level
     * of a NestedGraphTree object in an internal graph structure is equal to <b>height of the ROOT of the structure
     * wrt to deep vertices minus the depth of the object</b>. The hierarchical level concept is only relevant to
     * <b>deep vertices</b>, which are vertices which contain a graph within themselves that is non-empty. This
     * means that a vertex that has no graph within itself (i.e. a <b>shallow vertex</b>) cannot have a hierarchical
     * level. Some examples are included for better clarity:
     * <ul>
     *   <li>For a graph structure with only a root vertex 0, it has no concept of hierarchical level, since it
     *   doesn't have a graph nested within itself. This might be a bit odd to think of at first, but it's important
     *   to realise that the root vertex 0 is necessary to hold all other vertices, so if it has no children, there
     *   is no hierarchical levels to traverse.</li>
     *   <li>For a graph structure with a root vertex 0, and one child vertex 1, but the child has no nested graph
     *   withing itself, so it's not a deep vertex, the root has an hierarchical level of 0, because the height
     *   of the root wrt deep vertices is 0 (no deep vertices) and the depth is also 0, since it's a root.</li>
     *   <li>For a graph structure with a root vertex 0, with one child vertex 1, which has a child vertex 2, the
     *   hierarchical level of vertex 0 is 1 and of vertex 1 is 0.
     *   <ul>
     *       <li>
     *           The height of vertex 0 wrt deep vertices is 1, since it has only one deep vertex 'below' it,
     *           if vertex 2 was a deep vertex then the height of the root would have been 2. Its depth is 0
     *           since it's a root, so the level is (1-0) = 1. Important note: when calculating a hierarchical level
     *           we care about the height of the tree, so the height of the root. So we would need the height of the
     *           root no matter if we're looking at the hierarchical level of a root or a leaf vertex.
     *       </li>
     *       <li>
     *           The height of vertex 1's root wrt deep vertices is 1, there are no deep vertices below it.
     *           It's depth is also 1, since it's below the root with one step, so the hierarchical level is (1-1) = 0.
     *       </li>
     *       <li>
     *           As established, the hierarchical level of vertex 2 isn't defined, as it's a <b>shallow vertex</b>.
     *       </li>
     *   </ul>
     *   </li>
     * </ul>
     * @return hierarchical level of given NestedGraphTree
     */
    public int getHierarchicalLevel()
    {
        int depth = findDepth();

        NestedGraphTree root = this;
        while(!root.isRoot)
        {
            root = root.getParent();
        }
        int heightOfRoot = root.findHeight();
        return heightOfRoot - depth;
    }

    /**
     * Finds the depth of a the tree structure.
     * @return depth of tree
     */
    private int findDepth()
    {
        int depth = 0;
        NestedGraphTree currentVertex = this;

        while(!currentVertex.isRoot)
        {
            depth++;
            currentVertex = currentVertex.getParent();
        }

        return depth;
    }

    /**
     * Finds height of given the graph internal structure, however it only considers deep vertices when counting
     * height, so for example if we had a root vertex with one child that doesn't have a nested graph within itself,
     * then the height of the root is 0.
     * @return height of graph tree structure w.r.t. deep vertices
     */
    private int findHeight()
    {
        int height = -1;
        HashSet<NestedGraphTree> children = getChildren();
        for(NestedGraphTree child : children)
        {
            if(child.isVertexDeep())
            {
                height = Math.max(height, child.findHeight());
            }
        }
        return height + 1;
    }

    /**
     * Creates graph object from the graph in the NestedGraphTree object the method is called on, usually called when
     * the graph on screen needs to be changed so the visualisation viewer object requires a graph to visualise.
     * @return graph object from a NestedGraphTree object
     */
    public Graph<Integer,String> createGraph()
    {
        // Basic graph
        Graph<Integer, String> graph = new DirectedSparseGraph<>();

        // Get parent vertex by ID and it's vertices/edges
        ArrayList<Integer> currGraphVertices = getChildrenIds();
        HashSet<Edge> currGraphEdges = getEdges();

        // Vertices
        for (Integer currGraphVertex : currGraphVertices)
        {
            graph.addVertex(currGraphVertex);
        }

        // Edges
        for (Edge currGraphEdge : currGraphEdges)
        {
            graph.addEdge
                    (
                            currGraphEdge.getStartVertex().getId() +
                                    "-" + currGraphEdge.getDestinationVertex().getId(), // Edge name defined by
                            currGraphEdge.getStartVertex().getId(),                     // start and end vertices
                            currGraphEdge.getDestinationVertex().getId()
                    );
        }
        return graph;
    }

    /**
     * Getter for the information object of the NestedGraphTree object this method is called on, usually to display
     * in the additional information part of the right side screen.
     * @return information object of NestedGraphTree object
     */
    public InformationObject getInformationObject()
    {
        return informationObject;
    }

    /**
     * Setter for the parent of the NestedGraphTree object, usually called when a transformation is made that requires
     * the parent reference to change.
     * @param newParent the new parent of the NestedGraphTree object
     */
    public void setParent(NestedGraphTree newParent)
    {
        parent = newParent;
    }

    /**
     * Setter for the children collection of the NestedGraphTree object, usually called when a transformation is made
     * that requires the children to change.
     * @param newChildren the new children of the NestedGraphTree object
     */
    public void setChildren(HashSet<NestedGraphTree> newChildren)
    {
        children = newChildren;
    }

    /**
     * Setter for the edges withing the graph of the NestedGraphTree object, usually called when a transformation is
     * made that requires the edges to change.
     * @param newEdges the new edges of graph in the NestedGraphTree object
     */
    public void setEdges(HashSet<Edge> newEdges)
    {
        edges = newEdges;
    }

    /**
     * Checks if a NestedGraphTree object is "deep", so if it has children, to which it is sometimes referred to as a
     * "deep vertex/node", or if it's "shallow", or also called a "shallow vertex/node". Deep NestedGraphTree
     * objects have graphs within them, but shallow ones do not.
     * @return boolean to check if NestedGraphTree object has children
     */
    public boolean isVertexDeep()
    {
        return !children.isEmpty();
    }

    /**
     * Performs a graph traversal by counting each vertex so a counter of all vertices can be made, usually called on
     * root in the beginning so an accurate count of all vertices in the graph internal structure can be made and later
     * used to give out available id's to new vertices (likely resulting from a transformation, e.g. a merge).
     * @return count of all vertices of given tree
     */
    public int countVerticesInTree()
    {
        Stack<NestedGraphTree> stack = new Stack<NestedGraphTree>();
        stack.add(this);
        int counter = 0;

        while(!stack.isEmpty())
        {
            NestedGraphTree currVertex = stack.pop();
            counter++;

            if(currVertex.getChildren() != null)
            {
                stack.addAll(currVertex.getChildren());
            }
        }
        return counter;
    }

    /**
     * Overriden equals method needed for hash operations.
     * @param o - object to be compared with given reference
     * @return true if the same object, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NestedGraphTree that = (NestedGraphTree) o;
        return id == that.id &&
                isRoot == that.isRoot &&
                isMerged == that.isMerged &&
                Objects.equals(parent, that.parent) &&
                Objects.equals(children, that.children) &&
                Objects.equals(edges, that.edges) &&
                Objects.equals(informationObject, that.informationObject);
    }

    /**
     * Standard Java hashing for NestedGraphTree object.
     * @return hash code of object
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(id, isRoot, isMerged, informationObject);
    }

    /**
     * Goes through the whole internal graph structure to collect all vertices in the map.
     * @return hash map of all vertices
     */
    public HashMap<Integer, NestedGraphTree> collectAllVerticesInMap()
    {
        Stack<NestedGraphTree> stack = new Stack<NestedGraphTree>();
        stack.add(this);
        HashMap<Integer, NestedGraphTree> mapOfVertices = new HashMap<>();

        while(!stack.isEmpty())
        {
            NestedGraphTree currVertex = stack.pop();
            mapOfVertices.put(currVertex.hashCode(), currVertex);

            if(currVertex.getChildren() != null)
            {
                stack.addAll(currVertex.getChildren());
            }
        }
        return mapOfVertices;
    }
}
