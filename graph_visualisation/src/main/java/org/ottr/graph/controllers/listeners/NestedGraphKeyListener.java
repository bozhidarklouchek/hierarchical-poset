package org.ottr.graph.controllers.listeners;

import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import org.ottr.graph.models.representation.NestedGraphTree;
import org.ottr.transformations.models.Merge;
import org.ottr.graph.models.GraphModel;
import org.ottr.transformations.models.Transformation;
import org.ottr.graph.views.MainView;
import org.ottr.graph.views.VisualisationViewerTransformers;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Manages all events from key presses on the graph.
 */
public class NestedGraphKeyListener implements KeyListener
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public NestedGraphKeyListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {}

    /**
     * Currently, only pressing ctrl and the Z key will undo the last performed transformation.
     * If the last transformation is on the same graph as the user is currently, the transformation is undone.
     * If the last transformation isn't on the same graph, then the
     * {@link org.ottr.transformations.views.UndoTransformationDifferentViewerJDialog} is called which presents the user
     * with three choices:
     * <ul>
     *   <li>Go back to the graph where the transformation happened and undo it</li>
     *   <li>Don't go back, but undo the last transformation</li>
     *   <li>Cancel</li>
     * </ul>
     * If the user chooses the second option, but the current graph is a result of a transformation, so if it's undone
     * then the current graph will cease to exist, then the
     * {@link org.ottr.transformations.views.UndoTransformationMergeChildrenJDialog} is called which forces the user to
     * either go to the graph where the transformation happened and undo it or cancel.
     * @param e the event to be processed
     */
    @Override
    public void keyReleased(KeyEvent e)
    {
        // Undo is done via ctrl+z
        if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_Z)
        {
            // Do only if transformation history isn't empty
            if (!graphModel.getTransformationHistory().isTransformationHistoryEmpty())
            {
                Transformation lastTransformation = graphModel.getTransformationHistory().peekAtLastTransformation();

                if(lastTransformation.getType() == Transformation.transformationType.MERGE)
                {
                    // If user on same viewer as last merge
                    if (graphModel.getCurrentGraph().getId() == lastTransformation.getIdOfGraphTransformationOccurredOn())
                    {
                        Merge lastMerge = (Merge) graphModel.getTransformationHistory().popLastTransformation();

                        // Unselect all selected vertices so if we go back they're not selected
                        mainView.unselectAllVertices();
                        // Unselect all selected edges so if we go back they're not selected
                        mainView.unselectAllEdges();

                        graphModel.getCurrentGraph().setChildren(lastMerge.getOldGraphChildren());
                        graphModel.getCurrentGraph().setEdges(lastMerge.getOldGraphEdges());

                        // Fix parent links of children of merged vertex
                        for(NestedGraphTree child : graphModel.getCurrentGraph().getChildren())
                        {
                            for(NestedGraphTree childOfChild : child.getChildren())
                            {
                                childOfChild.setParent(child);
                            }
                        }

                        Graph<Integer, String> graphToDraw = graphModel.getCurrentGraph().createGraph();
                        // Create graph
                        Layout<Integer, String> layout = new ISOMLayout<>(graphToDraw);
                        mainView.getVisualizationViewer().setGraphLayout(layout);

                        // Add vertex and edge transformers
                        VisualisationViewerTransformers.addTransformers(mainView.getVisualizationViewer(), graphModel.getCurrentGraph(), graphModel.getMergedVertices());

                        // If merged nothing selected
                        mainView.getRightSideScreen().setVertexIdLabel("No vertex selected yet.");
                        mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
                        mainView.getRightSideScreen().getGoDownButton().setVisible(false);
                        mainView.getRightSideScreen().getMergeButton().setVisible(false);

                        mainView.drawGUI(graphModel.getCurrentGraph());
                    }
                    // If user NOT on same viewer as last merge
                    else
                    {
                        mainView.getUndoTransformationDifferentViewerJDialog().setVisible(true);
                    }
                }
            }
        }
    }
}
